try:
    grade = float(input("Enter a grade: "))
    if grade < 0 or grade > 1.0:
        print("Grade value must be between 0 and 1")
    elif grade >= 0.9:
        print("A")
    elif grade >= 0.8:
        print("B")
    elif grade >= 0.7:
        print("C")
    elif grade >= 0.6:
        print("D")
    else:
        print("F")
except ValueError:
    print("Please enter a number between 0 and 1")
